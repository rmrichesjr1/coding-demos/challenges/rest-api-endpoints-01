# tests.py

# The tests for the API project:

##############################################################################

# Python imports:

import json
import requests
import sys

##############################################################################

# Constants:

delimiter     = '/'

protocol = 'http'

host = 'localhost'

port = 52441

baseUrl = '%s://%s:%s' % ( protocol, host, port)

basePath = delimiter.join([ '', 'api', 'v1'])

statusNormal  = 200
statusCreated = 201

statusBadMethod = 405

##############################################################################

# Message printing utility functions:

# Set this to true to enable debug printing:
# enableDebugPrinting = False
enableDebugPrinting = True

def printDebug(msg):
    if ( not enableDebugPrinting):
        return
    print('DEBUG:     %s' % msg)

def printInfo(msg):
    print('     info: %s' % msg)

def printWarning(msg):
    print('  Warning: %s' % msg)

# NOTE: This function does not return.
def printError(msg):
    print('*** ERROR: %s' % msg)
    sys.exit(1)

##############################################################################

# Class of tests:

class Tests:

    def __init__(self):
        printDebug('Tests constructor starting:')
        printDebug('Tests constructor finished.')

    def doWork(self):
        printDebug('Tests doWork starting: %s' % baseUrl)



        self.dumpDb()



# FIXME: Code the rest of this.



        printDebug('Tests doWork finished')

    def dumpDb(self):
        printDebug('Tests dumpDb starting:')
        #
        # The entities page:
        theTop = self.getPage(baseUrl + basePath + '/')
        topPages = theTop['entities']
        #
        for topPage in topPages:
            printDebug('topPage: %s' % topPage)
            isCases  = (-1 != topPage.find('cases'))
            isSuites = (-1 != topPage.find('suites'))
            if (isCases == isSuites):
                printError('isCases and isSuites must be strong mutex.')
                # That function does not return.
            if (isCases):
                printInfo('found Cases:')
                which = 'test-cases'
            if (isSuites):
                printInfo('found Suites:')
                which = 'test-suites'
            theStruct = self.getPage(baseUrl + topPage)
            nextLevel = theStruct[which]
            ids   = nextLevel['ids']
            links = nextLevel['links']
            printInfo('      ids: %s' % ids)
            printInfo('    links: %s' % links)
            for link in links:
                thirdStruct = self.getPage(baseUrl + link)
                if (isCases):
                    self.dumpCase(thirdStruct)
                if (isSuites):
                    self.dumpSuite(thirdStruct)
        printDebug('Tests dumpDb finished.')

    # Return the unpacked structure from a page:
    def getPage( self, theUrl):
        printDebug('Tests getPage starting: %s' % theUrl)
        raw = requests.get(theUrl)
        status = raw.status_code
        if (statusNormal != status):
            printError('Page GET bad status: %d' % status)
            # That function does not return.
        result = json.loads(raw.text)
        printDebug('Tests getPage finished: %s' % result)
        return(result)

    # Dump a test case:
    def dumpCase( self, theStruct):
        printDebug('Tests dumpCase starting: %s' % theStruct)
        



        printDebug('Tests dumpCase finished.')

    # Dump a test suite:
    def dumpSuite( self, theStruct):
        printDebug('Tests dumpSuite starting: %s' % theStruct)
        



        printDebug('Tests dumpSuite finished.')

##############################################################################

if ('__main__' == __name__):
    Tests().doWork()

##############################################################################

# The End.
