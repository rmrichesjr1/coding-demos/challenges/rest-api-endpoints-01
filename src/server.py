# server.py

# The web server layer:

##############################################################################

# Python imports:

import flask
import json
import logging
import sys

from flask import Response

##############################################################################

# Our imports:

from store import Store                      # pylint: disable=relative-import

##############################################################################

# Constants:

delimiter     = '/'

pathApi       = delimiter.join([ '', 'api', 'v1'])

segmentCases  = 'test-cases'
segmentSuites = 'test-suites'

statusNormal  = 200
statusCreated = 201

statusBadMethod = 405

headersJson = { 'Content-Type': 'application/json' }
headersNone = {}

##############################################################################

# Message printing utility functions:

# Set this to true to enable debug printing:
# enableDebugPrinting = False
enableDebugPrinting = True

def printDebug(msg):
    if ( not enableDebugPrinting):
        return
    print('DEBUG:     %s' % msg)

def printInfo(msg):
    print('     info: %s' % msg)

def printWarning(msg):
    print('  Warning: %s' % msg)

# NOTE: This function does not return.
def printError(msg):
    print('*** ERROR: %s' % msg)
    sys.exit(1)

##############################################################################

# The Server class:

class Server:
    def __init__( self, port, theStore):
        printDebug('Server constructor starting:')
        self.port = port
        self.store = theStore
        printDebug('Server constructor finished.')

    # This is adapted from my Insteon lighting automation program:
    def launch(self):
        printDebug('Server launch starting:')

        app = flask.Flask(__name__)

        # Subfunctions to cause Flask to think it's route handler functions
        # are at module level rather than class level.

        ############################################

        @app.route( self.buildRoute([pathApi]),
                    methods = ['GET']
        )
        def subRoot():                       # pylint: disable=unused-variable
          return(self.routeRoot())

        @app.route( self.buildRoute([ pathApi, segmentCases]),
                    methods = [ 'GET', 'POST']
        )
        def subCases():                      # pylint: disable=unused-variable
          return(self.routeCases())

        # TODO: find a way to silence pylint unused variable complaint.
        @app.route( self.buildRoute([ pathApi, segmentCases, '<int:n>']),
                    methods = [ 'GET', 'DELETE', 'PUT']
        )
        def subCasesN(n):                    # pylint: disable=unused-variable
          return(self.routeCasesN(n))

        @app.route( self.buildRoute([ pathApi, segmentSuites]),
                    methods = [ 'GET', 'POST']
        )
        def subSuites():                     # pylint: disable=unused-variable
          return(self.routeSuites())

        # TODO: find a way to silence pylint unused variable complaint.
        @app.route( self.buildRoute([ pathApi, segmentSuites, '<int:n>']),
                    methods = [ 'GET', 'DELETE', 'PUT']
        )
        def subSuitesN(n):                   # pylint: disable=unused-variable
          return(self.routeSuitesN(n))

        # TODO: find a way to silence pylint unused variable complaint.
        @app.route( self.buildRoute([ pathApi, segmentSuites, '<int:n>',
                                     segmentCases]),
                    methods = ['POST']
        )
        def subSuitesNcases(n):              # pylint: disable=unused-variable
          return(self.routeSuitesNcases(n))

        # TODO: find a way to silence pylint unused variable complaint.
        @app.route( self.buildRoute([ pathApi, segmentSuites, '<int:n>',
                                     segmentCases, '<int:m>']),
                    methods = ['DELETE']
        )
        def subSuitesNcasesM( n, m):         # pylint: disable=unused-variable
          return(self.routeSuitesNcasesM(n, m))

        ############################################

        # Try to get Flask to quit spamming stdout/stderr.
        # This stops the spam when pages are loaded but not on Flask startup.
        if ( not enableDebugPrinting):
          log = logging.getLogger('werkzeug')
          log.setLevel(logging.ERROR)
          log.disabled = True
          app.logger.disabled = True
        #
        app.run( host='0.0.0.0', debug=enableDebugPrinting,
                 # The reloader only works in the main thread.
                 use_reloader=False,
                 port=self.port)
        printDebug('Server launch finished.')

    ############################################

    # Utility methods:

    # Build a route and be chatty about it if debug printing is enabled:
    #
    # If needed, this method appends the trailing slash to ensure the server
    # will respond as intended whether or not the client appends a trailing
    # slash.
    #
    def buildRoute( self, theParts):
        result = delimiter.join(theParts)
        if ( not result.endswith('/')):
            result += '/'
        printDebug('buildRoute: %s' % result)
        return(result)

    # Expand a DB case ID to a full REST path:
    def caseId2path( self, theId):
        return(delimiter.join([ pathApi, segmentCases, str(theId)]))

    # Expand a DB suite ID to a full REST path:
    def suiteId2path( self, theId):
        return(delimiter.join([ pathApi, segmentSuites, str(theId)]))

    # Expand DB case and suite IDs to a full REST path:
    def caseSuiteIds2path( self, suiteId, caseId):
        return(delimiter.join([ pathApi, segmentSuites, str(suiteId),
                                segmentCases, str(caseId)]))

    # Shrink a case REST path to a DB ID:
    def casePath2id( self, thePath):
        wantPrefix = delimiter.join([ pathApi, segmentCases, ''])
        if (thePath.startswith(wantPrefix)):
            # This might raise an exception:
            result = int(thePath[len(wantPrefix):])
            return(result)
        else:
            # Raise an exception ourselves:
            raise Exception('Invalid case path in casePath2id.')

    # Shrink a suite REST path to a DB ID:
    def suitePath2id( self, thePath):
        wantPrefix = delimiter.join([ pathApi, segmentSuites, ''])
        if (thePath.startswith(wantPrefix)):
            # This might raise an exception:
            result = int(thePath[len(wantPrefix):])
            return(result)
        else:
            # Raise an exception ourselves:
            raise Exception('Invalid suite path in suitePath2id.')

    # Shrink a suit/.../case REST path to a tuple of DB IDs:
    def suiteCasePath2ids( self, thePath):
        # This one is a little more complex than the two above.
        bad = 'Invalid suite/.../case path in suiteCasePath2ids.'
        #
        wantPrefix = delimiter.join([ pathApi, segmentCases, ''])
        if (thePath.startswith(wantPrefix)):
            # This might raise an exception:
            suffix = thePath[len(wantPrefix):]
            parts = suffix.split(delimiter)
            if (3 != len(parts)):
                # Raise an exception ourselves:
                raise Exception(bad)
            suiteId = int(parts[0])
            if (segmentCases != parts[1]):
                # Raise an exception ourselves:
                raise Exception(bad)
            caseId  = int(parts[2])
            result = ( suiteId, caseId)
            return(result)
        else:
            # Raise an exception ourselves:
            raise Exception(bad)

    # Manufacture a complete response object:
    def makeResponse( self, theStruct, theStatus, theHeaders):
        printDebug('Server makeResponse starting:')
        finalHeaders = {}
        for k in theHeaders:
          finalHeaders[k] = theHeaders[k]
        result = Response( json.dumps(theStruct), theStatus, theHeaders)
        printDebug('Server makeResponse finished: %s' % result)
        return(result)

    ############################################

    # GET, /
    def routeRoot(self):
        printDebug('Server routeRoot starting:')
        theMethod = flask.request.method
        if ('GET' == theMethod):
            printDebug('Get a list of the higher-level entities.')
            entities = [ self.buildRoute([ pathApi, segmentCases]),
                         self.buildRoute([ pathApi, segmentSuites])
                       ]
            result = self.makeResponse( { 'entities': entities},
                                        statusNormal, headersJson)
        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeRoot finished: %s' % result)
        return(result)

    # GET, POST, /test-cases/
    def routeCases(self):
        printDebug('Server routeCases starting:')
        theMethod = flask.request.method
        if ('GET' == theMethod):
            printDebug('Get a list of the test cases.')
            caseIds = self.store.getTestCases()
            links = []
            for caseId in caseIds:
                links.append(self.caseId2path(caseId))
            payload = { 'test-cases': {   'ids': caseIds,
                                        'links': links
                                      }
                      }
            result = self.makeResponse( payload, statusNormal, headersJson)
        elif ('POST' == theMethod):
            printDebug('Create a new test case.')

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1'}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeCases finished: %s' % result)
        return(result)
            
    # GET, DELETE, PUT, /test-cases/$n/
    def routeCasesN( self, n):
        printDebug('Server routeCasesN starting: %d' % n)
        theMethod = flask.request.method
        if ('GET' == theMethod):
            printDebug('Get information about a test case.')
            rawPayload = self.store.getTestCaseN(n),

# FIXME: Massage the results.
            finalPayload = rawPayload

            result = self.makeResponse( finalPayload,
                                        statusNormal, headersJson)
        elif ('DELETE' == theMethod):
            printDebug('Delete a test case.')

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1', 'N': n}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        elif ('PUT' == theMethod):
            printDebug('Modify/Replace an existing test case.')

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1', 'N': n}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeCasesN finished: %s' % result)
        return(result)

    # GET, POST, /test-suites/
    def routeSuites(self):
        printDebug('Server routeSuites starting:')
        theMethod = flask.request.method
        if ('GET' == theMethod):
            printDebug('Get a list of test suites.')
            suiteIds = self.store.getTestSuites()
            links = []
            for suiteId in suiteIds:
                links.append(self.suiteId2path(suiteId))
            payload = { 'test-suites': {   'ids': suiteIds,
                                         'links': links
                                       }
                      }
            result = self.makeResponse( payload, statusNormal, headersJson)
        elif ('POST' == theMethod):
            printDebug('Create a new test suite.')

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1'}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeSuites finished: %s' % result)
        return(result)

    # GET, DELETE, PUT, /test-suites/$n/
    def routeSuitesN( self, n):
        printDebug('Server routeSuitesN starting: %d' % n)
        theMethod = flask.request.method
        if ('GET' == theMethod):
            printDebug('Get information about a test suite.')
            rawPayload = self.store.getTestSuiteN(n)

# FIXME: Massage the results.
            finalPayload = rawPayload

            result = self.makeResponse( finalPayload,
                                        statusNormal, headersJson)
        elif ('DELETE' == theMethod):
            printDebug('Delete a test suite.')

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1', 'N': n}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        elif ('PUT' == theMethod):
            printDebug('Modify/Replace an existing test suite.')

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1', 'N': n}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeSuitesN finished: %s' % result)
        return(result)

    # POST, /test-suites/$n/test-cases/
    def routeSuitesNcases( self, n):
        printDebug('Server routeSuitesNcases starting: %d' % n)
        # In theory, method is know to be POST, but for belt and suspenders:
        theMethod = flask.request.method
        if ('POST' == theMethod):
            printDebug('Add a pre-existing test case to a test suite.')
            # It does _NOT_ create a new test case.

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1', 'N': n}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeSuitesNcases finished: %s' % result)
        return(result)

    # DELETE, /test-suites/$n/test-cases/$m/
    def routeSuitesNcasesM( self, n, m):
        printDebug('Server routeSuitesNcasesM starting: %d, %d' % ( n, m))
        theMethod = flask.request.method
        if ('DELETE' == theMethod):
            printDebug('Remove a pre-existing test case from a test suite.')
            # It does _NOT_ delete a test case itself.

# FIXME: Finish coding this:
            dummy = { 'key1': 'val1', 'N': n, 'M': m}
            result = self.makeResponse( dummy, statusNormal, headersJson)

        else:
            printDebug('Should not happen: not-allowed request method.')
            result = self.makeResponse( None, statusBadMethod, headersNone)
        printDebug('Server routeSuitesNcasesM finished: %s' % result)
        return(result)

##############################################################################

# The End.
