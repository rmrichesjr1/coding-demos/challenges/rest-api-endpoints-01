# store.py

# The DB storage layer:

# NOTE: In a real application where performance matters, connection(s) to the
# DB would be kept around.  For simplicity for this exercise, each transaction
# uses its own connection, cursor, etc.

##############################################################################

# Python imports:

import os
import sqlite3
import sys

##############################################################################

# Constants:

# Intending to run this from parent of 'src' and 'dbfiles'
dbFilename = 'dbfiles/theDatabase'

##############################################################################

# Message printing utility functions:

# Set this to true to enable debug printing:
# enableDebugPrinting = False
enableDebugPrinting = True

def printDebug(msg):
    if ( not enableDebugPrinting):
        return
    print('DEBUG:     %s' % msg)

def printInfo(msg):
    print('     info: %s' % msg)

def printWarning(msg):
    print('  Warning: %s' % msg)

# NOTE: This function does not return.
def printError(msg):
    print('*** ERROR: %s' % msg)
    sys.exit(1)

##############################################################################

# The Store class:

class Store:
    def __init__( self, filename):
        printDebug('Store constructor starting:')
        self.filename = filename
        # If the DB file does not exist, create it:
        if ( not os.path.exists(dbFilename)):
            self.buildDbFile(dbFilename)
        printDebug('Store constructor finished.')

    # Build a DB file.  The caller should check whether this is needed.
    def buildDbFile( self, theFilename):
        printDebug('Store buildDbFile starting: %s' % theFilename)
        con = sqlite3.connect(theFilename)
        cur = con.cursor()
        #
        cur.execute('''
            CREATE TABLE cases
            ( caseId serial primary key,
              name text not null,
              steps text not null
            )
        ''')
        #
        cur.execute('''
            CREATE TABLE suites
            ( suiteId serial primary key,
              name text not null
            )
        ''')
        #
        cur.execute('''
            CREATE TABLE caseSuites
            (  caseId integer not null references cases  ( caseId),
              suiteId integer not null references suites (suiteId)
            )
        ''')
        #
        con.commit()
        con.close()
        printDebug('Store buildDbFile finished.')

    # Return a list of test case IDs:
    def getTestCases(self):
        printDebug('Store getTestCases starting:')
        con = sqlite3.connect(dbFilename)
        cur = con.cursor()
        #
        result = []
        for row in cur.execute(''' SELECT caseId FROM cases '''):
            result.append(row[0])
        #
        con.commit()
        con.close()
        printDebug('Store getTestCases finished: %s' % result)
        return(result)

    # Return info about a specified test case:
    #
    # A list:
    #     id
    #     name
    #     steps
    #
    def getTestCaseN( self, n):
        printDebug('Store getTestCaseN starting:')
        con = sqlite3.connect(dbFilename)
        cur = con.cursor()
        #
        selector = (n,)
        #
        cur.execute('''
            SELECT caseId, name, steps
            FROM cases
            WHERE caseId = ?
        ''',
        selector)
        #
        onlyRow = cur.fetchall()[0]
        result = [ onlyRow[0], onlyRow[1], onlyRow[2]]
        #
        con.commit()
        con.close()
        printDebug('Store getTestCaseN finished: %s' % result)
        return(result)

    # Return a list of test suite IDs:
    def getTestSuites(self):
        printDebug('Store getTestSuites starting:')
        con = sqlite3.connect(dbFilename)
        cur = con.cursor()
        #
        result = []
        for row in cur.execute(''' SELECT suiteId FROM suites '''):
            result.append(row[0])
        #
        con.commit()
        con.close()
        printDebug('Store getTestSuites finished: %s' % result)
        return(result)
        
    # Return info about a specified test suite:
    #
    # A list:
    #     id
    #     name
    #     list of case IDs
    #
    def getTestSuiteN( self, n):
        printDebug('Store getTestSuiteN starting:')
        con = sqlite3.connect(dbFilename)
        cur = con.cursor()
        #
        selector = (n,)
        #
        cur.execute('''
            SELECT suiteId, name
            FROM suites
            WHERE suiteId = ?
        ''',
        selector)
        #
        onlyRow = cur.fetchall()[0]
        resultId   = onlyRow[0]
        resultName = onlyRow[1]
        #
        # Now, get the case IDs:
        rows = cur.execute('''
            SELECT caseId
            FROM   caseSuites
            WHERE  suiteId = ?
        ''',
        selector)
        #
        resultCases = []
        for row in rows:
            resultCases.append(row[0])
        #
        result = [ resultId, resultName, resultCases]
        #
        con.commit()
        con.close()
        printDebug('Store getTestSuiteN finished: %s' % result)
        return(result)



##############################################################################

# The End.
