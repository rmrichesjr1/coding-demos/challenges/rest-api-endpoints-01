# main.py

# Top-level:

##############################################################################

# Python imports:



##############################################################################

# Our imports:

from server import Server                    # pylint: disable=relative-import
from store  import Store                     # pylint: disable=relative-import

##############################################################################

# Constants:

port = 52441

databaseName = 'test-management.sql3'

##############################################################################

# Bootstrap function:

def doTopLevel():
    Server( port, Store(databaseName)).launch()

##############################################################################

if ('__main__' == __name__):
    doTopLevel()

##############################################################################

# The End.
